
echo upgrading "$1" "$2"

#cd ../linux-provisioning-includes

mkdir -p tracking/"$1"/"$2"

for f in $(find $1"/"$2 -maxdepth 1  -name 'v[0-9][0-9][0-9][0-9]-*.sh'  | sort ); do 
  #echo $f;
  PART1=`dirname "$f"`
  PART2=`basename "$f"`
  
  TRACK="tracking/$f"
  
  #echo "Found... "$f
  
  if [ ! -f $TRACK ]; then
    
    echo "Running... "$f
    (cd $PART1 && exec sudo bash ./$PART2)
    echo "ran" > $TRACK
  fi
  
done


#cd ../linux-provisioning
