#!/bin/sh

while test $# -gt 0; do
  case "$1" in
    -h|--help)
      echo "$package - attempt to capture frames"
      echo " "
      echo "$package [options] application [arguments]"
      echo " "
      echo "options:"
      echo "-h, --help                   show brief help"
      
      echo "-m, --machine=MACHINE        specify the machine to provision (required)"
      echo "-r, --repository=REPOSITORY  specify the repository to read from (default bitbucket.org)"
      echo "-c, --company=COMPANY        specify the company/namespace to read from (default base2art)"
      echo "-p, --product=PRODUCT        specify the product/subspace  to read from (default 'server-provisioning-{CurrentOsName}')"
      echo "-s, --sku=VERSION            specify the sku to install (default: non-sku)"
      exit 0
      ;;
    -m)
      shift
      if test $# -gt 0; then
        export MACHINE=$1
      else
        echo "no process specified"
        exit 1
      fi
      shift
      ;;
    --machine*)
      export MACHINE=`echo $1 | sed -e 's/^[^=]*=//g'`
      shift
      ;;
    -r)
      shift
      if test $# -gt 0; then
        export REPOSITORY=$1
      else
        echo "no output dir specified"
        exit 1
      fi
      shift
      ;;
    --repository*)
      export REPOSITORY=`echo $1 | sed -e 's/^[^=]*=//g'`
      shift
      ;;
    -c)
      shift
      if test $# -gt 0; then
        export COMPANY=$1
      else
        echo "no output dir specified"
        exit 1
      fi
      shift
      ;;
    --company*)
      export COMPANY=`echo $1 | sed -e 's/^[^=]*=//g'`
      shift
      ;;
    -p)
      shift
      if test $# -gt 0; then
        export PRODUCT=$1
      else
        echo "no output dir specified"
        exit 1
      fi
      shift
      ;;
    --product*)
      export PRODUCT=`echo $1 | sed -e 's/^[^=]*=//g'`
      shift
      ;;
    -v)
      shift
      if test $# -gt 0; then
        export VERSION=$1
      else
        echo "no output dir specified"
        exit 1
      fi
      shift
      ;;
    --version*)
      export VERSION=`echo $1 | sed -e 's/^[^=]*=//g'`
      shift
      ;;
    *)
      break
      ;;
  esac
done




mkdir -p machines

if [ ! -f machines/"$MACHINE" ]; then
  ssh -t $MACHINE -o "StrictHostKeyChecking no" "cat /etc/os-release"  >> machines/"$MACHINE"
fi


OS_ID=`awk -F= '$1=="ID" { print $2 ;}' machines/"$MACHINE"  | awk -F'"' '{print $2}'`
OS_VERSION=`awk -F= '$1=="VERSION_ID" { print $2 ;}' machines/"$MACHINE"  | awk -F'"' '{print $2}'`
OS="${OS_ID}-${OS_VERSION}"



export MY_REPO="${REPOSITORY:-bitbucket.org}"
export MY_COMPANY="${COMPANY:-base2art}"
export MY_PRODUCT="${PRODUCT:-server-provisioning-${OS}}"
export MY_VERSION="${VERSION:-.}"

#echo $MACHINE
#echo $MY_REPO
#echo $MY_COMPANY
#echo $MY_PRODUCT
#echo $VERSION


mkdir -p ../linux-provisioning-includes/"$MY_REPO"/"$MY_COMPANY"


CODE_NAME="$MY_REPO"/"$MY_COMPANY"/"$MY_PRODUCT"
CODE_DIR=../linux-provisioning-includes/"$CODE_NAME"

if [ -d "$CODE_DIR"/.git ]; then
  echo "Exists"
  git -C  $CODE_DIR pull
else
  git clone git@"$MY_REPO":"$MY_COMPANY"/"$MY_PRODUCT".git $CODE_DIR
fi


TODAY=`date +%Y-%m-%d.%H-%M-%S`
TAR_NAME="$MACHINE"-"$TODAY".tar.gz
TAR_PATH=machines/"$TAR_NAME"

#echo $TAR_PATH
TAR_COMMAND="rm -rf "$MY_REPO";tar xzf "$TAR_NAME""
tar -C ../linux-provisioning-includes -czf  $TAR_PATH  $CODE_NAME
scp -o "StrictHostKeyChecking no" $TAR_PATH tyoung@$MACHINE:.
ssh -t $MACHINE -o "StrictHostKeyChecking no" $TAR_COMMAND


scp -o "StrictHostKeyChecking no" ./upgrade.sh tyoung@$MACHINE:.


#DEFAULT UPGRADE
UPG_COMMAND="sh ./upgrade.sh "$CODE_NAME" ."
ssh -t $MACHINE -o "StrictHostKeyChecking no" $UPG_COMMAND


if [ $MY_VERSION != "." ]; then
  
  # SKU UPGRADE
  UPG_COMMAND="sh ./upgrade.sh "$CODE_NAME" "$VERSION""
  ssh -t $MACHINE -o "StrictHostKeyChecking no" $UPG_COMMAND

fi









#~ if [ ! machines/"$MACHINE"-"$".tar.gz ]; then
  #~ tar czf name_of_archive_file.tar.gz name_of_directory_to_tar
#~ fi

#
##scp -r "$MY_REPO"/"$MY_COMPANY"/"$MY_PRODUCT" user@$MACHINE:"$MY_REPO"/"$MY_COMPANY"/"$MY_PRODUCT"
#
## https://leathakkor@bitbucket.org/base2art/server-provisioning-centos-7.git
#GIT_REPO="https://"$MY_REPO"/"$MY_COMPANY"/"$MY_PRODUCT".git"
##GIT_REPO="git@"$MY_REPO":"$MY_COMPANY"/"$MY_PRODUCT".git"
#GIT_LOCATION="$MY_REPO"/"$MY_COMPANY"/"$MY_PRODUCT"
#
##echo $GIT_REPO
##echo $GIT_LOCATION
#
#GIT_COMMAND="git clone "$GIT_REPO" "$GIT_LOCATION""
#
#echo $GIT_COMMAND
#
#SSH_COMMAND="ssh-keyscan "$MY_REPO" >> ~/.ssh/known_hosts"
#
#ssh -t $MACHINE -o "StrictHostKeyChecking no" 'sudo yum -y install git'
##ssh -t $MACHINE -o "StrictHostKeyChecking no" ""$SSH_COMMAND""
#ssh -t $MACHINE -o "StrictHostKeyChecking no" ""$GIT_COMMAND""
#
